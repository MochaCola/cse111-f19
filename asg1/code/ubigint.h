// $Id: ubigint.h,v 1.11 2016-03-24 19:43:57-07 - - $

#ifndef __UBIGINT_H__
#define __UBIGINT_H__

#include <exception>
#include <iostream>
#include <limits>
#include <utility>
#include <vector>
using namespace std;

#include "debug.h"
#include "relops.h"

class ubigint {
   friend ostream& operator<< (ostream&, const ubigint&);
   
   private:
      using udigit = int;
      using ubigvalue = vector<udigit>;
   public:
      ubigvalue uvalue;
      int size = uvalue.size();
      
      void multiply_by_2();
      void divide_by_2();
      void trim(int keep);
      //ubigvalue fill(ubigvalue uvalue, int num);

      ubigint() = default; // Need default ctor as well.
      ubigint (unsigned long);
      ubigint (const string&);

      ubigint operator+ (const ubigint&) const;
      ubigint operator- (const ubigint&) const;
      ubigint operator* (const ubigint&) const;
      ubigint operator/ (const ubigint&) const;
      ubigint operator% (const ubigint&) const;

      bool operator== (const ubigint&) const;
      bool operator<  (const ubigint&) const;
      
      //friend ostream& operator<< (ostream& out, const ubigint& that);
};

#endif
