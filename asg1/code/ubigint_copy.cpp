// $Id: ubigint.cpp,v 1.16 2019-04-02 16:28:42-07 - - $

#include <cctype>
#include <cstdlib>
#include <exception>
#include <math.h>
#include <stack>
#include <stdexcept>
#include <string>
using namespace std;

#include "ubigint.h"
#include "debug.h"

ubigint::ubigint (unsigned long that): uvalue (that) {
   //DEBUGF ('~', this << " -> " << uvalue)
   //cout << "Long constructor" << endl;
   string value = to_string(that);
   for (char digit: value) {
        if (not isdigit (digit)) {
           throw invalid_argument ("ubigint::ubigint(" + value + ")");
        }
        //cout << "digit: " << digit << "\tdigit - '0': " << digit - '0' << "\n";
        uvalue.push_back(digit - '0');
   }
}

ubigint::ubigint (const string& that): uvalue(0) {
   //DEBUGF ('~', "that = \"" << that << "\"");
   //cout << "String constructor" << endl;
   for (char digit: that) {
      if (not isdigit (digit)) {
         throw invalid_argument ("ubigint::ubigint(" + that + ")");
      }
      //cout << "digit: " << digit << "\tdigit - '0': " << digit - '0' << "\n";
      uvalue.push_back(digit - '0');
   }
}

ubigint ubigint::operator+ (const ubigint& that) const {
   ubigint result {0};
   int carry = 0, keep = 0;
   int size1 = uvalue.size(), size2 = that.uvalue.size();
   int top_size = (size1 > size2) ? size1 : size2;
   int low_size = (size1 < size2) ? size1 : size2;
   int space = (size1 > size2) ? (size1 - size2) : (size2 - size1);
   bool b_dig1 = (size1 > size2) ? true : false;
   bool b_dig2 = (size1 < size2) ? true : false;
   //cout << "b_dig1: " << b_dig1 << "\tb_dig2: " << b_dig2 << endl;
   result.uvalue.resize(top_size);
   //cout << "uvalue.size(): " << uvalue.size() << "\tthat.uvalue.size(): " << that.uvalue.size() << endl;
   for (int i = (low_size - 1); i >= 0; i--) {
        int dig1, dig2;
        try {
           dig1 = uvalue.at(i + (b_dig1 * space));
           //cout << "uvalue.at(" << i + (b_dig1 * space) << ") = " << dig1 << endl; 
        } catch (const std::out_of_range& error) {
           dig1 = 0;
        } try {
           dig2 = that.uvalue.at(i + (b_dig2 * space));
           //cout << "that.uvalue.at(" << i + (b_dig2 * space) << ") = " << dig2 << endl; 
        } catch (const std::out_of_range& error) {
           dig2 = 0;
        }
        //cout << "dig1: " << dig1 << "\tdig2: " << dig2 << "\tcarry: " << carry << endl;
        int sum = dig1 + dig2 + carry;
        if ((sum == 0 || sum == 10) && i == low_size - keep - 1) keep += 1;
        //cout << "sum: " << sum << "\tkeep: " << keep << endl;
        carry = 0;
        if (sum >= 10 && i != 0) {
           sum -= 10;
           carry = 1;
           //cout << "pushing back sum (" << sum << ")...\n";
           result.uvalue.push_back(sum);
           //cout << "result: " << result << endl;
        } else if (sum >= 10 && i == 0 && size1 == size2) {
           //cout << "##### i must equal zero when this message is seen. #####\n";
           string val = to_string(sum);
           //cout << "val: " << val << "\tval[1]: " << val[1] << "\tval[0]: " << val[0] << endl;
           //cout << "pushing_back val[1] (" << val[1] << ")...\n";
           result.uvalue.push_back(val[1] - '0');
           //cout << "result: " << result << endl;
           //cout << "pushing_back val[0] (" << val[0] << ")...\n";
           result.uvalue.push_back(val[0] - '0');
           //cout << "result: " << result << endl;
        } else {
           if (sum >= 10) {
              sum -= 10;
              carry = 1;
           }
           //cout << "pushing_back " << sum << "...\n";
           result.uvalue.push_back(sum);
        }
   }
   for (int i = space - 1; i >= 0; i--) {
      if (b_dig1) result.uvalue.push_back(uvalue.at(i));
      if (b_dig2) result.uvalue.push_back(that.uvalue.at(i));
   }
   result.trim(keep);
   cout << result << endl;
   return result;
}

ubigint ubigint::operator- (const ubigint& that) const {
   //if (*this < that) throw domain_error ("ubigint::operator-(a<b)");
   ubigint result {0};
   int borrow = 0, keep = 0;
   int size1 = uvalue.size(), size2 = that.uvalue.size();
   int top_size = (size1 > size2) ? size1 : size2;
   for (int i = (top_size - 1); i >= 0; i--) {
        int dig1 = uvalue.at(i);
        int dig2 = that.uvalue.at(i);
        cout << "dig1: " << dig1 << "\tdig2: " << dig2 << "\tborrow: " << borrow << endl;
        int dif = dig1 - dig2 - borrow--;
        if (dif == 0 && i == top_size - keep - 1) keep += 1;
        cout << "dif: " << dif << "\tkeep: " << keep << endl;
        borrow = 0;
        if (dif < 0 && i != 0) {
           if (dif != 0) dif += 10;
           borrow = 1;
        }
        result.uvalue.push_back(dif);
   }
   result.trim(keep);
   cout << "result: " << result << endl;
   return result;
}

ubigint ubigint::operator* (const ubigint& that) const {
   ubigint result {0};
   int this_place = 1, that_place = 1;
   for (int i = size; i > 0; i--) {
      for (int j = that.size; j > 0; j--) {
         long part_prod = (uvalue.at(i)) * (that.uvalue.at(j))
                          * this_place * that_place;
         ubigint prod(part_prod);
         result = result + prod;
         that_place *= 10;
      }
      this_place *= 10;
   }
   return result;
}

void ubigint::multiply_by_2() {
   int result = 0, carry = 0;
   for (int i = size; i > 0; i--) {
      result = ((uvalue.at(i)) * 2) + carry--;
      if (result >= 10) {
         result -= 10;
         carry++;
      }
      uvalue.at(i) = result;
   }
}

void ubigint::divide_by_2() {
   ubigint result {0};
   int exp = size - 1;
   for (int i = 0; i < size; i++) {
      result = result + ((uvalue.at(i) * pow(10, exp--)) / 2);
   }
   uvalue = result.uvalue;
}

void ubigint::trim(int keep) {
   //cout << "Trimming function" << endl;
   int size_v = uvalue.size(), i = 0;
   while (uvalue.at(i++) == 0 && i != size_v) cout << "i: " << i << endl;
   //cout << "before: " << *this << endl;
   //cout << "erasing from uvalue.begin() to uvalue.begin() + i - 1 - keep (" << keep << ")...\n";
   uvalue.erase(uvalue.begin(), uvalue.begin() + (i - 1 - keep));
   //cout << "after: " << *this << endl;
}

/*
ubigvalue ubigint::fill(ubigvalue uvalue, int num) {
   cout << "Filling function" << endl;
   ubigint result {0};
   result.uvalue.resize(that.uvalue.size() + num);
   while (num-- != 0) result.uvalue.push_back(0);
   for (int i = 0; i < that.uvalue.size(); i++) {
      result.uvalue.push_back(that.uvalue.at(i));
   }
   cout << "after filling: " << result << endl;
   return result.uvalue;
}
*/


struct quo_rem { ubigint quotient; ubigint remainder; };
quo_rem udivide (const ubigint& dividend, const ubigint& divisor_) {
   // NOTE: udivide is a non-member function.
   ubigint divisor {divisor_};
   ubigint zero {0};
   if (divisor == zero) throw domain_error ("udivide by zero");
   ubigint power_of_2 {1};
   ubigint quotient {0};
   ubigint remainder {dividend}; // left operand, dividend
   while (divisor < remainder) {
      divisor.multiply_by_2();
      power_of_2.multiply_by_2();
   }
   while (power_of_2 > zero) {
      if (divisor <= remainder) {
         remainder = remainder - divisor;
         quotient = quotient + power_of_2;
      }
      divisor.divide_by_2();
      power_of_2.divide_by_2();
   }
   return {.quotient = quotient, .remainder = remainder};
}

ubigint ubigint::operator/ (const ubigint& that) const {
   return udivide (*this, that).quotient;
}

ubigint ubigint::operator% (const ubigint& that) const {
   return udivide (*this, that).remainder;
}

bool ubigint::operator== (const ubigint& that) const {
   return uvalue == that.uvalue;
}

bool ubigint::operator< (const ubigint& that) const {
   return uvalue < that.uvalue;
}

ostream& operator<< (ostream& out, const ubigint& that) {
   for (int i = that.uvalue.size() - 1; i >= 0; i--) {
      out << that.uvalue.at(i);
   }
   return out;
}
