// $Id: bigint.cpp,v 1.78 2019-04-03 16:44:33-07 - - $

#include <cstdlib>
#include <exception>
#include <stack>
#include <stdexcept>
#include <vector>
using namespace std;

#include "bigint.h"
#include "debug.h"
#include "relops.h"
#include "ubigint.h"

bigint::bigint (long that): uvalue (that), is_negative (that < 0) {
   //DEBUGF ('~', this << " -> " << uvalue)
}

bigint::bigint (const ubigint& uvalue_, bool is_negative_):
                uvalue(uvalue_), is_negative(is_negative_) {
}

bigint::bigint (const string& that) {
   is_negative = that.size() > 0 and that[0] == '_';
   uvalue = ubigint (that.substr (is_negative ? 1 : 0));
}

bigint bigint::operator+ () const {
   return *this;
}

bigint bigint::operator- () const {
   return {uvalue, not is_negative};
}

bigint bigint::operator+ (const bigint& that) const {
   bigint result;
   if (is_negative == that.is_negative) {
      result.uvalue = uvalue + that.uvalue;
      result.is_negative = is_negative;
   }
   else {
      if (uvalue == that.uvalue) {
         result.uvalue = that.uvalue - uvalue;
         result.is_negative = false;
      } else if (uvalue > that.uvalue) {
         result.uvalue = uvalue - that.uvalue;
         result.is_negative = is_negative;
      } else if (uvalue < that.uvalue) {
         result.uvalue = that.uvalue - uvalue;
         result.is_negative = that.is_negative;
      }
   } cout << result << endl;
   return result;
}

bigint bigint::operator- (const bigint& that) const {
   bigint result = uvalue - that.uvalue;
   //cout << result << endl;
   return result;
}


bigint bigint::operator* (const bigint& that) const {
   bigint result = uvalue * that.uvalue;
   return result;
}

bigint bigint::operator/ (const bigint& that) const {
   bigint result = uvalue / that.uvalue;
   return result;
}

bigint bigint::operator% (const bigint& that) const {
   bigint result = uvalue % that.uvalue;
   return result;
}

bool bigint::operator== (const bigint& that) const {
   //return is_negative == that.is_negative and uvalue == that.uvalue;
   bool sign1 = is_negative;
   bool sign2 = that.is_negative;
   if (not (sign1 == sign2) and (size == that.size)) return false;
   else {
      for (int i = 0; i < size; i++) {
         if (not (uvalue.uvalue.at(i) == that.uvalue.uvalue.at(i)))
            return false;
      } return true;
   }
}

bool bigint::operator< (const bigint& that) const {
   if (is_negative != that.is_negative) return is_negative;
   //return is_negative ? uvalue > that.uvalue
   //                   : uvalue < that.uvalue;
   else if (not (is_negative && that.is_negative)) {
      if (size < that.size) return true;
      else if (size > that.size) return false;
      else for (int i = 0; i < size; i++) {
         if (uvalue.uvalue.at(i) != that.uvalue.uvalue.at(i)) {
            return (uvalue.uvalue.at(i) < that.uvalue.uvalue.at(i));
         }
      }
   }
   else {
      if (size < that.size) return false;
      else if (size > that.size) return true;
      else for (int i = 0; i < size; i++) {
         if (uvalue.uvalue.at(i) != that.uvalue.uvalue.at(i)) {
            return (uvalue.uvalue.at(i) > that.uvalue.uvalue.at(i));
         }
      }
   }
   return is_negative;
}

ostream& operator<< (ostream& out, const bigint& that) {
   return out << (that.is_negative ? "-" : "") << that.uvalue;
}
