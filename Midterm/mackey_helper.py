import urllib.request
import re
import sys

# USAGE: python3 mackey_helper.py WORD-DUMP OUTPUT ACCURACY [cmps104a, cmps112, cmps109, other]
# Accuracy matches what percentage match of past test questions
# IE: 0.8 would match test questions that 80% of the words are on this word dump
from urllib.error import URLError
from collections import OrderedDict
# from nltk.corpus import stopwords

# stop_words = set(stopwords.words('english'))

if len(sys.argv) < 5:
    exit("Not enough arguments! \n Usage is: python3 mackey_helper.py WORD-DUMP OUTPUT ACCURACY [cmps104a, cmps112, cmps109, other]")

wordList = []

try:
    inp = open(sys.argv[1], "r")
    wordList = inp.read().split()
    inp.close()
except FileNotFoundError:
    exit("Word dump does not exist, please specify the file properly!")

output = open(sys.argv[2], "w")
accuracy_required = float(sys.argv[3])

matchingText = "Matching questions with at least " + str(accuracy_required * 100) + "% in common."
print(matchingText)
output.write(matchingText + '\n\n')

wordList = [word.strip() for word in wordList] # if word.strip() not in stop_words]
print(wordList)

BASE_DIR = "https://www2.ucsc.edu/courses/" + sys.argv[4] + "-wm/:/Old-Exams/"
TESTS = []
try:
    website = urllib.request.urlopen(BASE_DIR)
    html = website.read().decode('utf-8')
    TESTS = re.findall(r'href="(.*?-midterm\.tt)"', html)
    TESTS.extend(re.findall(r'href="(.*?-final\.tt)"', html))
except URLError:
    exit("Could not find " + BASE_DIR + "!")

print("Remember, solutions are at " + BASE_DIR + ".solutions!")

TESTS = list(OrderedDict.fromkeys(TESTS))

for TEST in TESTS:
    URL = BASE_DIR + TEST
    txt = ""

    try:
        txt = urllib.request.urlopen(URL).read().decode("ISO-8859-1")
    except ValueError:
        print("Could not open request to " + URL + ", incorrectly parsed!")
    except URLError:
        print("Could not find " + URL + "!")
        continue

    print("File is", TEST)
    txt_split = txt.split("Multiple choice")
    free_response = txt_split[0]
    multiple_choice = txt_split[1:]

    fr = re.compile(r"\n *[0-9]+\.").split("".join(free_response))[1:]
    mc = []
    for val in multiple_choice:
        mc.append(re.compile(r"\n *[0-9]+\.").split("".join(val))[1:])

    index = 1
    for elem in fr:
        num_matched = 0
        num_tot = 0
        temp = str(elem).strip().split()
        temp = [word.lower() for word in temp] # if word.lower() not in stop_words]
        matched_list = []
        for t in temp:
            tmp = re.sub(r'[.\';,()\[\]\\\n:]', '', t)
            if tmp.isalpha():
                if tmp in wordList:
                    num_matched += 1
                    matched_list.append(tmp)
                num_tot += 1

        if num_tot == 0:
            index += 1
            continue
        if num_matched / num_tot > accuracy_required:
            output.write("Question " + str(index) + " on Free Response on " + TEST + " with accuracy_required " + str(
                num_matched / num_tot) + ", words " + str(matched_list) + ": " + '\n' + (len(str(index)) + 1) * " " + elem + '\n\n\n')
        index += 1
    for part, each in enumerate(mc):
        index = 1
        for elem in each:
            num_matched = 0
            num_tot = 0
            temp = str(elem).strip().split()
            matched_list = []
            for tmp in temp:
                tmp = tmp.strip()
                if tmp.isalpha() or tmp.isdigit():
                    if tmp in wordList:
                        num_matched += 1
                        matched_list.append(tmp)
                    num_tot += 1
            if num_tot == 0:
                index += 1
                continue
            if num_matched / num_tot > accuracy_required:
                output.write("Question " + str(index) + " on Multiple Choice part " + str(
                    part + 1) + " on " + TEST + " with accuracy_required " + str(num_matched / num_tot) + ", words " + str(matched_list) + ": \n" + (
                                     len(str(len(each))) + 1) * " " + elem + '\n\n\n')
            index += 1
output.close()
