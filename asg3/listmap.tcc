// $Id: listmap.tcc,v 1.15 2019-10-30 12:44:53-07 - - $

#include "listmap.h"
#include "debug.h"

//
/////////////////////////////////////////////////////////////////
// Operations on listmap.
/////////////////////////////////////////////////////////////////
//

//
// listmap::~listmap()
//
template <typename key_t, typename mapped_t, class less_t>
listmap<key_t,mapped_t,less_t>::~listmap() {
   DEBUGF ('l', reinterpret_cast<const void*> (this));
   while (not empty()) {
      erase(begin());
   }
}

//
// iterator listmap::insert (const value_type&)
//
template <typename key_t, typename mapped_t, class less_t>
typename listmap<key_t,mapped_t,less_t>::iterator
listmap<key_t,mapped_t,less_t>::insert (const value_type& pair) {
   DEBUGF ('l', &pair << "->" << pair);
   node* item = new node(nullptr, nullptr, pair);
   iterator it = begin();
   /*
   while(it != end()) {   
      if (not less(it->first, pair.first)) break;
      ++it;
   }
   */
   for( ; it != end(); ++it) {
      if (not less(it->first, pair.first)) break;
   }
   if (it != end()) {
      if (not less(pair.first, it->first) &&
          not less(it->first, pair.first))
          it = erase(it);
   }
   item->next = it.where;
   item->prev = it.where->prev;
   it.where->prev->next = item;
   it.where->prev = item;
   return iterator(item);
}

//
// listmap::find(const key_type&)
//
template <typename key_t, typename mapped_t, class less_t>
typename listmap<key_t,mapped_t,less_t>::iterator
listmap<key_t,mapped_t,less_t>::find (const key_type& that) {
   DEBUGF ('l', that);
   for (auto it = iterator(anchor()->next); it != end(); ++it) {
      if (it->first == that) return it;
   }
   return end();
}

//
// iterator listmap::erase (iterator position)
//
template <typename key_t, typename mapped_t, class less_t>
typename listmap<key_t,mapped_t,less_t>::iterator
listmap<key_t,mapped_t,less_t>::erase (iterator position) {
   DEBUGF ('l', &*position);
   position.where->next->prev = position.where->prev;
   position.where->prev->next = position.where->next;
   delete position.where;
   ++position;
   return position;
}


