// $Id: main.cpp,v 1.11 2018-01-25 14:19:29-08 - - $

#include <cstdlib>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>
#include <unistd.h>

using namespace std;

#include "listmap.h"
#include "xpair.h"
#include "util.h"

using str_str_map = listmap<string,string>;
using str_str_pair = str_str_map::value_type;

const string cin_name = "-";
str_str_map map;

void scan_options (int argc, char** argv) {
   opterr = 0;
   for (;;) {
      int option = getopt (argc, argv, "@:");
      if (option == EOF) break;
      switch (option) {
         case '@':
            debugflags::setflags (optarg);
            break;
         default:
            complain() << "-" << char (optopt) << ": invalid option"
                       << endl;
            break;
      }
   }
}

void perform(istream& file, string name) {
   string line, key, value;
   int no = 0;
   for (;;) {
      if (file.eof()) break;
      getline(file, line);
      if (line.empty() || line.front() == '#') continue;
      no++;
      cout << name << ": " << no << ": " << line << endl;
      if (not line.empty()) {
         line = line.substr(line.find_first_not_of(" "), line.size());
         line = line.substr(0, line.find_last_not_of("\n") + 1);
      }
      key = line.substr(0, line.find_first_of("="));
      while (key.back() == ' ') key.pop_back();
      if (static_cast<int>(line.find("=")) != -1) {
         value = line.substr(line.find_first_of("=") + 1, line.size());
         while (value.front() == ' ') value = value.substr(1);
      } else value = "";
      if (line.front() == '=') {
         for (auto it = map.begin(); it != map.end(); ++it) {
            if (value.size() != 0 && it->second != value) continue;
            else cout << it->first << " = " << it->second << endl;
         }
      } else if (line.front() != '#') {
         auto pair = map.find(key);
         if (value.size() == 0) {
            if (static_cast<int>(line.find("=")) != -1) {
               if (pair != map.end()) pair = map.erase(pair);
            }
            else {
               if (pair != map.end()) {
                  cout << pair->first << " = " << pair->second << endl;
               }
               else cerr << key << ": key not found" << endl;
            }
         }
         else {
            const str_str_pair new_pair (key, value);
            pair = map.insert(new_pair);
            cout << pair->first << " = " << pair->second << endl;
         }
      }
   }
}

int main (int argc, char** argv) {
   sys_info::execname(argv[0]);
   scan_options (argc, argv);
   if (argc > 1) {
      for (int i = 1; i < argc; ++i) {
         string name = argv[i];
         if (name == cin_name) perform(cin, cin_name);
         else {
            ifstream file(name);
            if (file.fail()) {
               cerr << sys_info::execname() << ": " << name
                    << ": No such file or directory" << endl;
            }
            else {
               perform(file, name);
               file.close();
            }
         }
      }
   }
   else perform(cin, cin_name);
   return sys_info::exit_status();
}
