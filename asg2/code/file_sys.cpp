// $Id: file_sys.cpp,v 1.7 2019-07-09 14:05:44-07 - - $

#include <iostream>
#include <stdexcept>
#include <unordered_map>
#include <algorithm>

using namespace std;

#include "debug.h"
#include "file_sys.h"

int inode::next_inode_nr{1};

struct file_type_hash {
    size_t operator()(file_type type) const {
        return static_cast<size_t> (type);
    }
};

ostream &operator<<(ostream &out, file_type type) {
    static unordered_map<file_type, string, file_type_hash> hash{
            {file_type::PLAIN_TYPE,     "PLAIN_TYPE"},
            {file_type::DIRECTORY_TYPE, "DIRECTORY_TYPE"},
    };
    return out << hash[type];
}

inode_state::inode_state() {
    DEBUGF ('i', "root = " << root << ", cwd = " << cwd
                           << ", prompt = \"" << prompt() << "\"");
    // create initial root inode
    root = make_shared<inode>(file_type::DIRECTORY_TYPE);
    //set cwd to root
    cwd = root;
    //add entries to the inode's contents dirents where those entires are keyed by '.' and '..'
    //and their values will be the root inode ptr for both

    root->contents->insert_into_dirents(".", root);
    root->contents->insert_into_dirents("..", root);
}

inode_state::~inode_state() {
    //here you enter the recursion to recursively delete contents from ALL the inodes
}

const string &inode_state::prompt() const { return prompt_; }

void inode_state::set_prompt(string x) {
    prompt_ = x;
}

ostream &operator<<(ostream &out, const inode_state &state) {
    out << "inode_state: root = " << state.root
        << ", cwd = " << state.cwd;
    return out;
}

inode::inode(file_type type) : inode_nr(next_inode_nr++) {
    switch (type) {
        case file_type::PLAIN_TYPE:
            contents = make_shared<plain_file>();
            break;
        case file_type::DIRECTORY_TYPE:
            contents = make_shared<directory>();
            break;
    }
    DEBUGF ('i', "inode " << inode_nr << ", type = " << type);
}


int inode::get_inode_nr() const {
    DEBUGF ('i', "inode = " << inode_nr);
    return inode_nr;
}


file_error::file_error(const string &what) :
        runtime_error(what) {}

const wordvec &base_file::readfile() const {
    throw file_error("is a " + error_file_type());
}

void base_file::writefile(const wordvec &) {
    throw file_error("is a " + error_file_type());
}

void base_file::remove(const string &) {
    //  throw file_error("is a " + error_file_type());
}

inode_ptr base_file::mkdir(const string &) {
    throw file_error("is a " + error_file_type());
}

inode_ptr base_file::mkfile(const string &) {
    throw file_error("is a " + error_file_type());
}

void base_file::insert_into_dirents(const string &s, inode_ptr p) {
    throw file_error("is a " + error_file_type());
}

void directory::insert_into_dirents(const string &s, inode_ptr p) {
    dirents[s] = p;
}


size_t plain_file::size() const {
    size_t size{0};
    for (size_t i = 0; i < data.size(); i++) {
        size = size + data[i].length();

    }
    if (data.size() > 0) {
        size = size + (data.size() - 1);
    }
    DEBUGF ('i', "size = " << size);
    return size;
}

const wordvec &plain_file::readfile() const {
    DEBUGF ('i', data);
    return data;
}

void plain_file::writefile(const wordvec &words) {
    DEBUGF ('i', words);
    data = words;
}

size_t directory::size() const {
    size_t size{dirents.size()};
    DEBUGF ('i', "size = " << size);
    return size;
}

void directory::remove(const string &filename) {
    DEBUGF ('i', filename);
    dirents.erase(filename);
}

inode_ptr directory::mkdir(const string &dirname) {
    DEBUGF ('i', dirname);
    auto temp = make_shared<inode>(file_type::DIRECTORY_TYPE);
    dirents.insert({dirname, temp});
    auto ptr = static_pointer_cast<directory>(temp->get_contents());
    ptr->insert_into_dirents(".", temp);
    return temp;
}

inode_ptr directory::mkfile(const string &filename) {
    DEBUGF ('i', filename);
    auto temp = make_shared<inode>(file_type::PLAIN_TYPE);
    dirents.insert({filename, temp});
    return temp;
}

inode_ptr directory::resolve_path(string &path, inode_state &state) {
    size_t num = 0;
    string token;
    inode_ptr ptr;
    auto map = dirents;
    //for (auto it = map.begin(); it != map.end(); ++it) {
        //cout << it->first << endl;
    //} cout << endl;
    if (path == "/") return state.get_root();
    if (path.front() == '/') path.erase(0, 1);
    if (path.back() != '/') path.push_back('/');
    while ((num = path.find('/')) != string::npos) {
        token = path.substr(0, num);
        //cout << "after substr: " << token << endl;
        if (map.find(token) != map.end()) {
            //cout << token << " found :)\n";
            ptr = map.find(token)->second;
            if (ptr->get_contents()->is_dir()) {
                map = static_pointer_cast<directory>(ptr->get_contents())->dirents;
            } else return nullptr;
        }
        else {
            //cout << token << " not found :(\n";
            return nullptr;
        }
        path.erase(0, num + 1);
    }
    return ptr;
}







