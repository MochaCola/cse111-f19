// $Id: commands.cpp,v 1.18 2019-10-08 13:55:31-07 - - $

#include "commands.h"
#include <iostream>
#include "debug.h"
#include <string>

command_hash cmd_hash{
        {"cat",    fn_cat},
        {"cd",     fn_cd},
        {"echo",   fn_echo},
        {"exit",   fn_exit},
        {"ls",     fn_ls},
        {"lsr",    fn_lsr},
        {"make",   fn_make},
        {"mkdir",  fn_mkdir},
        {"prompt", fn_prompt},
        {"pwd",    fn_pwd},
        {"rm",     fn_rm},
        {"rmr",    fn_rmr},
        {"#",      fn_comment}
};

command_fn find_command_fn(const string &cmd) {
    // Note: value_type is pair<const key_type, mapped_type>
    // So: iterator->first is key_type (string)
    // So: iterator->second is mapped_type (command_fn)
    DEBUGF ('c', "[" << cmd << "]");
    const auto result = cmd_hash.find(cmd);
    if (result == cmd_hash.end()) {
        throw command_error(cmd + ": no such function");
    }
    return result->second;
}

command_error::command_error(const string &what) :
        runtime_error(what) {
}

int exit_status_message() {
    int status = exec::status();
    cout << exec::execname() << ": exit(" << status << ")" << endl;
    return status;
}

void fn_cat(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() < 2) { throw command_error("No file specified\n"); }
    auto dir = static_pointer_cast<directory>(state.get_cwd()->get_contents())->get_dirents();
    if (dir.find(words[1]) != dir.end()) {
       if (dir.find(words[1])->second->get_contents()->is_dir()) {
          throw command_error("Cannot cat a directory\n");
       }
       for (size_t i = 1; i < words.size(); i++) {
          auto y = dir.find(words[i]);
          if (y != dir.end()) {
             auto z = y->second->get_contents()->readfile();
             for (size_t j = 0; j < z.size(); j++) {
                cout << z[j];
                if (j != z.size() - 1) {
                    cout << " ";
                }
             }
             cout << endl;
          } else {
             throw command_error("No such file or directory found\n");
          }
       }
    }
    else {
       throw command_error("File doesn't exist\n");
    }
}

void fn_cd(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() == 2) {
        string wordy = words[1];
        if (wordy.back() != '/') { wordy.push_back('/'); }
        auto temp = state.get_cwd()->get_contents();
        auto x = static_pointer_cast<directory>(temp);
        auto boi = x->resolve_path(wordy, state);
        if (boi == nullptr) {
            throw command_error("Cannot resolve path (path contains plainfile or does not exist)\n");
        } else { state.set_cwd(boi); }
    } else if (words.size() == 1) {
        state.set_cwd(state.get_root());
    } else {
        throw command_error("cd: Too many arguments");
    }
}

void fn_echo(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    cout << word_range(words.cbegin() + 1, words.cend()) << endl;
}

void fn_exit(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    throw ysh_exit();
}

void fn_ls(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    base_file_ptr temp;
    if (words.size() == 1) {
        temp = state.get_cwd()->get_contents();
    } else if (words.size() == 2 && (words[1] == "/" || words[1] == "//")) {
        temp = state.get_root()->get_contents();
    } else if (words.size() == 2) {
        auto contents = state.get_cwd()->get_contents();
        auto dir = static_pointer_cast<directory>(contents);
        string path = words[1];
        temp = (dir->resolve_path(path, state))->get_contents();
        if (temp == nullptr) { throw command_error("Invalid path\n"); }
    } else {
        throw command_error("Too many arguments\n");
    }
    auto x = (static_pointer_cast<directory>(temp));
    auto dirents = x->get_dirents();
    if (state.get_cwd() != state.get_root() && x->bruh.back() == '/') {
        x->bruh.pop_back();
    }
    cout << x->bruh << ":" << endl;
    for (auto it = dirents.begin(); it != dirents.end(); ++it) {
        cout << it->second->get_inode_nr() << "\t" <<
             it->second->get_contents()->size() << "\t" << it->first <<
             ((it->second->get_contents()->is_dir() && it->first != "." && it->first != "..") ? "/" : "") << endl;
    }
}

void fn_lsr(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    auto cwd = state.get_cwd();
    if (words.size() == 1) {
        do_lsr(state, words);
    } else if (words.size() == 2) {
        auto dir = static_pointer_cast<directory>(state.get_cwd()->get_contents());
        string path = words[1];
        auto ptr = dir->resolve_path(path, state);
        if (ptr == nullptr) { throw command_error("Invalid path\n"); }
        state.set_cwd(ptr);
        do_lsr(state, {"lsr"});
    }
    state.set_cwd(cwd);
}

void do_lsr(inode_state &state, const wordvec &words) {
    fn_ls(state, words);
    bool stop = false;
    auto &our_state = state;
    while (stop == false) {
        bool found = false;
        auto dir = static_pointer_cast<directory>(our_state.get_cwd()->get_contents());
        auto conts = dir->get_dirents();
        for (auto it = conts.begin(); it != conts.end(); ++it) {
            if ((it->second->get_contents()->is_dir()) && (it->first != ".") && (it->first != "..")) {
                found = true;
                our_state.set_cwd(it->second);
                fn_ls(our_state, words);
            }
        }
        if (not found) stop = true;
    }
}

void fn_make(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() >= 2) {
        if (num = words[1].find('/') != words[1].end()) {
           string filename = words[1];
           while (num = filename.find('/') != string::npos);
           string pathname = filename.substr(0, num);
           filename.erase(0, num);
           auto dir = static_pointer_cast<directory>(state.get_cwd()->get_contents());
           auto ptr = dir->resolve_path(state, pathname);
           state.set_cwd(ptr);
           fn_make(state, {"make", filename});
        }
        auto temp = state.get_cwd()->get_contents();
        auto x = (static_pointer_cast<directory>(temp));
        auto dir = x->get_dirents();
        if (dir.find(words[1]) != dir.end()) {
            auto ptr = dir.find(words[1])->second;
            if (ptr->get_contents()->is_dir()) {
                throw command_error("File is already a directory\n");
            } else {
                auto file = static_pointer_cast<plain_file>(ptr->get_contents());
                wordvec new_words = words;
                new_words.erase(new_words.begin(), new_words.begin() + 2);
                file->writefile(new_words);
            }
        }
        auto ptr = x->mkfile(words[1]);
        wordvec new_vec{};
        for (int i = 2; i != words.size(); i++) {
            new_vec.push_back(words[i]);
        }
        ptr->get_contents()->writefile(new_vec);
    } else {
        throw command_error("No argument\n");
    }

}

void fn_mkdir(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() == 2) {
        string name = words[1];
        if (words[1].back() == '/') {
            name.pop_back();
        }
        auto temp = state.get_cwd()->get_contents();
        auto x = (static_pointer_cast<directory>(temp));
        auto ptr = x->mkdir(name);
        auto next = static_pointer_cast<directory>(ptr->get_contents());
        next->bruh = x->bruh + name + "/";
        ptr->get_contents()->insert_into_dirents("..", state.get_cwd());
    } else if (words.size() < 2) {
        throw command_error("No directory specified\n");
    } else {
        throw command_error("Too many arguments\n");
    }
}

void fn_prompt(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() < 2) {
        throw command_error("No argument specified\n");
    } else {
        state.set_prompt("");
        for (int i = 1; i < words.size(); i++) {
            state.set_prompt(state.prompt() + words[i] + " ");
        }
    }
}

void fn_pwd(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() > 1) {
        cout << "Too many arguments\n";
    } else {
        if (state.get_cwd() == state.get_root()) {
            cout << "/" << endl;
        }
        if (state.get_cwd() != state.get_root()) {
            auto temp = state.get_cwd()->get_contents();
            auto x = static_pointer_cast<directory>(temp);
            cout << x->bruh << endl;
        }
    }
}

void fn_rm(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() < 2) {
        throw command_error("No argument specified\n");
    } else if (words.size() > 2) {
        throw command_error("Too many arguments\n");
    } else {
        auto temp = state.get_cwd()->get_contents();
        auto x = (static_pointer_cast<directory>(temp));
        auto dirents = x->get_dirents();
        for (size_t i = 1; i < words.size(); i++) {
            if (dirents.find(words[i]) != dirents.end()) {
                x->remove(words[i]);
            }
        }
    }
}

void fn_rmr(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    if (words.size() == 1) { throw command_error("No file path specified"); }
    if (words.size() == 2) {
        auto begin = state.get_cwd();
        auto dir = static_pointer_cast<directory>(state.get_cwd()->get_contents());
        string name = words[1];
        auto ptr = dir->resolve_path(name, state);
        if (ptr == nullptr) throw command_error("Invalid path\n");
        state.set_cwd(ptr);
        dir = static_pointer_cast<directory>(state.get_cwd()->get_contents());
        auto map = dir->get_dirents();
        for (auto it = map.begin(); it != map.end(); ++it) {
            if (not it->second->get_contents()->is_dir()) {
                fn_rm(state, {"rm", it->first});
            }
        }
        for (auto it = map.begin(); it != map.end(); ++it){
            if (it->second->get_contents()->is_dir() && it->first != "." && it->first != "..") {
                fn_rmr(state, {"rmr", it->first});
                state.set_cwd(begin);
                fn_rm(state, {"rm", it->first});
            }
        }
    } else if (words.size() > 2) {
       throw command_error("Too many arguments\n");
    }
}

void fn_comment(inode_state &state, const wordvec &words) {
    DEBUGF ('c', state);
    DEBUGF ('c', words);
    // intentionally left empty
}
