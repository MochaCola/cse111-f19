// Kenny Blum (kpblum@ucsc.edu)
// Claire Camomile (ccamomil@ucsc.edu)

// $Id: cix.cpp,v 1.9 2019-04-05 15:04:28-07 - - $

#include <iostream>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>
#include <fstream>

using namespace std;

#include <libgen.h>
#include <sys/types.h>
#include <unistd.h>

#include "protocol.h"
#include "logstream.h"
#include "sockets.h"

logstream outlog(cout);
struct cix_exit : public exception {
};

unordered_map<string, cix_command> command_map{
        {"exit", cix_command::EXIT},
        {"help", cix_command::HELP},
        {"ls",   cix_command::LS},
        {"get",  cix_command::GET},
        {"put",  cix_command::PUT},
        {"rm",   cix_command::RM},

};

static const char help[] = R"||(
exit         - Exit the program.  Equivalent to EOF.
get filename - Copy remote file to local host.
help         - Print help summary.
ls           - List names of files on remote server.
put filename - Copy local file to remote host.
rm filename  - Remove file from remote server.
)||";

void cix_help() {
    cout << help;
}

void cix_ls(client_socket &server) {
    cix_header header;
    header.command = cix_command::LS;
    outlog << "sending header " << header << endl;
    send_packet(server, &header, sizeof header);
    recv_packet(server, &header, sizeof header);
    outlog << "received header " << header << endl;
    if (header.command != cix_command::LSOUT) {
        outlog << "sent LS, server did not return LSOUT" << endl;
        outlog << "server returned " << header << endl;
    } else {
        auto buffer = make_unique<char[]>(header.nbytes + 1);
        recv_packet(server, buffer.get(), header.nbytes);
        outlog << "received " << header.nbytes << " bytes" << endl;
        buffer[header.nbytes] = '\0';
        cout << buffer.get();
    }
}

void cix_get(client_socket &server, const string &filename) {
    cix_header header;
    header.command = cix_command::GET;
    for (size_t i = 0; i < filename.size(); ++i) {
        header.filename[i] = filename[i];
    }
    outlog << "Sending header " << header << endl;
    send_packet(server, &header, sizeof header);
    recv_packet(server, &header, sizeof header);
    outlog << "Received header " << header << endl;
    if (header.command != cix_command::FILEOUT) {
        outlog << "Header is: " << header.filename;
        outlog << "Sent GET, server did not return FILEOUT" << endl;
        outlog << "Server returned " << header << endl;
    } else {
        char *buffer = new char[header.nbytes + 1];
        if (header.nbytes == 0) ;
        else recv_packet(server, buffer, header.nbytes);
        outlog << "Received " << header.nbytes << " bytes" << endl;
        buffer[header.nbytes] = '\0';
        ofstream outfile{header.filename};
        outfile.write(buffer, header.nbytes);
        outfile.close();
        cout << header.filename << " was made\n";
    }
}

void cix_put(client_socket &server, const string &filename) {
    cix_header header;
    header.command = cix_command::PUT;
    for (size_t i = 0; i < filename.size(); ++i) {
        header.filename[i] = filename[i];
    }
    ifstream infile {header.filename};
    if (not infile.is_open()) outlog << "No existing file\n";
    else {
        string buffer;
        for (;;) {
           char c = infile.get();
           if (static_cast<int>(c) != -1) buffer.push_back(c);
           else break;
        }
        header.nbytes = buffer.size();
        infile.close();
        outlog << "Sending header " << header << endl;
        send_packet(server, &header, sizeof header);
        send_packet(server, buffer.c_str(), buffer.size());
        outlog << "Sent " << buffer.size() << " bytes\n";
        if (header.nbytes == 0) ;
        else recv_packet(server, &header, sizeof header);
        outlog << "Received header " << header << endl;
        if (header.command != cix_command::ACK) {
            outlog << "Sent CIX_PUT, server did not return"
                   << " CIX_AK\n";
            outlog << "server returned " << header << endl;
        }
    }
}

void cix_rm(client_socket &server, const string &filename){
    cix_header header;
    header.command = cix_command::RM;
    for(size_t i = 0; i < filename.size(); ++i){
        header.filename[i] = filename[i];
    }
    outlog << "Sending header " << header << endl;
    send_packet(server, &header, sizeof header);
    recv_packet(server, &header, sizeof header);
    outlog << "Received header " << header << endl;
    if(header.command != cix_command::ACK){
        outlog << "Sent CIX_RM, server did not return CIX_ACK\n";
    }
}


void usage() {
    cerr << "Usage: " << outlog.execname() << " [host] [port]" << endl;
    throw cix_exit();
}

int main(int argc, char **argv) {
    outlog.execname(basename(argv[0]));
    outlog << "starting" << endl;
    vector<string> args(&argv[1], &argv[argc]);
    if (args.size() > 2) usage();
    string host = get_cix_server_host(args, 0);
    in_port_t port = get_cix_server_port(args, 1);
    outlog << to_string(hostinfo()) << endl;
    try {
        outlog << "connecting to " << host << " port " << port << endl;
        client_socket server(host, port);
        outlog << "connected to " << to_string(server) << endl;
        for (;;) {
            string line;
            string filename;
            int place;
            getline(cin, line);
            if (line.find(" ") != string::npos) {
                place = line.find(" ");
                filename = line.substr(place + 1);
                line = line.substr(0, place);
            }
            if (cin.eof()) throw cix_exit();
            outlog << "command " << line << endl;
            const auto &itor = command_map.find(line);
            cix_command cmd = itor == command_map.end()
                              ? cix_command::ERROR : itor->second;
            switch (cmd) {
                case cix_command::EXIT:
                    throw cix_exit();
                    break;
                case cix_command::HELP:
                    cix_help();
                    break;
                case cix_command::LS:
                    cix_ls(server);
                    break;
                case cix_command::GET:
                    cix_get(server, filename);
                    break;
                case cix_command::PUT:
                    cix_put(server, filename);
                    break;
                case cix_command::RM:
                    cix_rm(server, filename);
                    break;
                default:
                    outlog << line << ": invalid command" << endl;
                    break;
            }
        }
    } catch (socket_error &error) {
        outlog << error.what() << endl;
    } catch (cix_exit &error) {
        outlog << "caught cix_exit" << endl;
    }
    outlog << "finishing" << endl;
    return 0;
}

